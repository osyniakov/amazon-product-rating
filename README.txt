
							Amazon Product Rating

What is it?
-----------

Amazon Product Rating is a tool that helps to gather product rating depending on specific keywords on amazon.com.

System Requirements
-------------------

JRE:
	1.8.66 or above.
Mozilla Firefox, Portable Edition:
	46.0.1
Operating System:
	Windows:
		Windows XP or above.

Installing Amazon Product Rating
----------------

1) Download and install JRE from:
	http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html

	* for Windows x86 download jre-8u92-windows-i586.exe
	* for Windows x64 download jre-8u92-windows-x64.exe

2) Download and install Mozilla Firefox 46.0.1 from:
	* for Windows (Portable Edition): http://downloads.sourceforge.net/portableapps/FirefoxPortable_46.0.1_English.paf.exe
    * for Linux: https://ftp.mozilla.org/pub/firefox/releases/46.0.1/linux-x86_64/en-US/firefox-46.0.1.tar.bz2
    * for MacOS: https://ftp.mozilla.org/pub/firefox/releases/46.0.1/mac/en-US/Firefox%2046.0.1.dmg

3) Unpack the archive where you would like to store the binaries, e.g.:
	unzip amazon-product-rating-x-RELEASE-y.zip

2) A directory called "amazon-product-rating-x-RELEASE" will be created.

3) Rename "config.properties.example" to "config.properties" and edit following parameters:

	- Path to input CSV file. WARNING: Output data will be stored in new columns of input file.
		inFilePath=d:\\WORK\\AmazonProductRating\\\files\ProductRatingExample.csv

	- Debug mode. For enable set true.
		debugMode=true

	- Debug folder path. In this folder screenshots of pages will be stored.
		debugFolderPath=d:\\WORK\\AmazonProductRating\\debug\\

	- Path to installed Mozilla Firefox (See step 2)
		firefoxPath=d:\\Programs\\firefox\\FirefoxPortable46\\FirefoxPortable.exe

	- Max search deph. For example, if product has rating 110 program will stop on 100 and won't go futher.
		maxSearchDeph=100

	- Amount of parallel working threads.
		threads=5

	- Cron expression indicates that product rating should be gathered hourly.
		cronExpression=0 0 0/1 * * ?

4) Run "startup.bat" to verify that it is correctly installed.

5) Do not close window with name "Amazon Product Rating". Program will gather statistic every hour automatically.

