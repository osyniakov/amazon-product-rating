package com.wwu.onlineshop;

import com.wwu.onlineshop.provider.AmazonSearchContext;
import com.wwu.onlineshop.provider.AmazonSearchResult;
import com.wwu.onlineshop.provider.AmazonSearchResult.Typ;
import com.wwu.onlineshop.provider.ProductRatingProvider;
import com.wwu.onlineshop.provider.webdriver.WebDriverProductRatingProvider;
import com.wwu.onlineshop.util.Pair;
import org.apache.commons.io.IOUtils;
import org.quartz.CronScheduleBuilder;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListReader;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class AmazonProductRating {

	private static final Logger LOG = LoggerFactory.getLogger(AmazonProductRating.class);

	// Category, Keywords, Product ASIN
	private static final CellProcessor[] CELL_PROCESSORS = new CellProcessor[] {new NotNull(), new NotNull(), new NotNull()};
	private static final String RATING_COLUMN = "Rating";
	private static final String RATING_SPONSORED_COLUMN = "Rating (Sponsored)";

	private void processFile(String inFilename) {
		String executionId = executionId();
		AmazonProductRatingConfig config = AmazonProductRatingConfig.INSTANCE;
		int maxSearchDeph = AmazonProductRatingConfig.INSTANCE.getMaxSearchDeph();
		String debugFolderPath = provideDebugFolderPath(AmazonProductRatingConfig.INSTANCE.getDebugFolderPath(), executionId);

		Map<Integer, Pair<AmazonSearchContext, AmazonSearchResult>> result = new HashMap<>();

		BlockingQueue<ProductRatingProvider> providersPool = new ArrayBlockingQueue<>(AmazonProductRatingConfig.INSTANCE.getThreads(), false);
		for (int i = 0; i < AmazonProductRatingConfig.INSTANCE.getThreads(); i++) {
			providersPool.add(new WebDriverProductRatingProvider(config.getFirefoxPath()));
		}

		ICsvListReader listReader;
		try {
			listReader = new CsvListReader(new FileReader(inFilename), CsvPreference.EXCEL_PREFERENCE);
			String[] header = listReader.getHeader(true);
			List<List<String>> lines = new ArrayList<>();
			List<String> l;
			while ((l = listReader.read()) != null) {
				lines.add(l);
			}
			listReader.close();

			ExecutorService executor = Executors.newCachedThreadPool();
			int lineNum = 1;
			for (List<String> line : lines) {
				AmazonSearchContext searchContext = new AmazonSearchContext();
				searchContext.setLineNum(lineNum);
				searchContext.setCategory(String.valueOf(line.get(0)));
				searchContext.setKeyword(String.valueOf(line.get(1)));
				searchContext.setTargetProductAsin(String.valueOf(line.get(2)));
				searchContext.setMaxSearchDeph(maxSearchDeph);
				searchContext.setDebugFolderPath(debugFolderPath);
				executor.execute(new ProductRatingRunnable(searchContext, providersPool, result));
				lineNum++;
			}
			executor.shutdown();
			executor.awaitTermination(1, TimeUnit.DAYS);
			providersPool.forEach(x -> x.shutdown());
			writeResult(header, lines, inFilename, result, executionId);
		} catch (Exception e) {
			LOG.error("Error occured", e);
		}
	}

	private void writeResult(String[] header, List<List<String>> lines, String outFilename, Map<Integer, Pair<AmazonSearchContext, AmazonSearchResult>> result, String executionId) {
		ICsvListWriter listWriter = null;
		try {
			listWriter = new CsvListWriter(new FileWriter(outFilename), CsvPreference.EXCEL_PREFERENCE);

			List<String> headerAsList = new ArrayList<>(Arrays.asList(header));
			headerAsList.add(resultColumn(RATING_COLUMN, executionId));
			headerAsList.add(resultColumn(RATING_SPONSORED_COLUMN, executionId));
			listWriter.writeHeader(headerAsList.toArray(new String[0]));

			int lineNum = 1;
			for (List<String> line : lines) {
				Pair<AmazonSearchContext, AmazonSearchResult> resultPair = result.get(lineNum);
				line.add(String.valueOf(getRating(resultPair.getSecond(), Typ.NORMAL)));
				line.add(String.valueOf(getRating(resultPair.getSecond(), Typ.SPONSORED)));
				listWriter.write(line);
				lineNum++;
			}

		} catch (IOException e) {
			LOG.error("Error occured", e);
		} finally {
			IOUtils.closeQuietly(listWriter);
		}
	}

	private String resultColumn(String columnName, String executionId) {
		return columnName + " " + executionId;
	}

	public int getRating(AmazonSearchResult result, Typ typ) {
		return result.getTyp() == typ && result.getStatus().isOk() ? result.getRating() : AmazonSearchResult.UNDEFINED.getRating();
	}

	private String provideDebugFolderPath(String path, String executionId) {
		return path + executionId + File.separator;
	}

	private String executionId() {
		return new SimpleDateFormat("YYYY.MM.dd_HH_mm").format(new Date());
	}

	public static void main(String[] args) {
		try {
			AmazonProductRatingConfig.INSTANCE.load(AmazonProductRating.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (Exception e) {
			LOG.error("Invalid properties file", e);
			System.exit(0);
		}

		runScheduled();
		run();
	}

	private static void runScheduled() {
		try {
			JobKey jobKey = JobKey.jobKey("ratingJob");
			JobDetail job = JobBuilder.newJob(RatingJob.class)
							.withIdentity(jobKey)
							.build();
			Trigger trigger = TriggerBuilder.newTrigger()
								.withSchedule(CronScheduleBuilder.cronSchedule(AmazonProductRatingConfig.INSTANCE.getCronExpression())
										.withMisfireHandlingInstructionDoNothing())
								.build();
			Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(job, trigger);
			scheduler.triggerJob(jobKey);
		} catch (Exception e) {
			LOG.error("Problem with scheduler", e);
		}
	}

	public static void run() {
		AmazonProductRating rating = new AmazonProductRating();
		AmazonProductRatingConfig config = AmazonProductRatingConfig.INSTANCE;
		rating.processFile(config.getInFilePath());
	}

	@DisallowConcurrentExecution
	public static class RatingJob implements Job {

		@Override
		public void execute(JobExecutionContext ctx) throws JobExecutionException {
			run();
		}

	}

}
