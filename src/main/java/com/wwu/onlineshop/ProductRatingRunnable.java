package com.wwu.onlineshop;

import java.util.Map;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wwu.onlineshop.provider.AmazonSearchContext;
import com.wwu.onlineshop.provider.AmazonSearchResult;
import com.wwu.onlineshop.provider.ProductRatingProvider;
import com.wwu.onlineshop.util.Pair;

public class ProductRatingRunnable implements Runnable {

	private static final Logger LOG = LoggerFactory.getLogger(ProductRatingRunnable.class);

	private AmazonSearchContext searchContext;
	private BlockingQueue<ProductRatingProvider> providersPool;
	private Map<Integer, Pair<AmazonSearchContext, AmazonSearchResult>> result;

	public ProductRatingRunnable(AmazonSearchContext searchContext, BlockingQueue<ProductRatingProvider> providersPool, Map<Integer, Pair<AmazonSearchContext, AmazonSearchResult>> result) {
		this.searchContext = searchContext;
		this.providersPool = providersPool;
		this.result = result;
	}

	@Override
	public void run() {
		ProductRatingProvider provider = null;
		try {
			provider = providersPool.take();
			provider.initialize();
			AmazonSearchResult searchResult = provider.getResult(searchContext);
			LOG.debug("-------------------------------------------");
			LOG.debug(String.valueOf(provider));
			LOG.debug(String.valueOf(searchContext));
			LOG.debug(String.valueOf(searchResult));
			result.put(searchContext.getLineNum(), new Pair<>(searchContext, searchResult));
		} catch (Exception e) {
			LOG.error("Provider error occured", e);
		} finally {
			try {
				providersPool.put(provider);
			} catch (InterruptedException e) {
				LOG.error("Error on adding to pool", e);
			}
		}
	}

}
