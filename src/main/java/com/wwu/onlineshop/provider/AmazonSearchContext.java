package com.wwu.onlineshop.provider;

public class AmazonSearchContext {

	private int lineNum;
	private String category;
	private String keyword;
	private String targetProductAsin;
	private int maxSearchDeph;
	private String debugFolderPath;

	@Override
	public String toString() {
		return "AmazonSearchContext [lineNum=" + lineNum + ", "
				+ "category=" + category + ", "
				+ "keyword=" + keyword + ", "
				+ "targetProductAsin=" + targetProductAsin + ", "
				+ "maxSearchDeph=" + maxSearchDeph + ", "
				+ "debugFolderPath=" + debugFolderPath + "]";
	}

	public int getLineNum() {
		return lineNum;
	}

	public void setLineNum(int lineNum) {
		this.lineNum = lineNum;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getTargetProductAsin() {
		return targetProductAsin;
	}

	public void setTargetProductAsin(String targetProductAsin) {
		this.targetProductAsin = targetProductAsin;
	}

	public int getMaxSearchDeph() {
		return maxSearchDeph;
	}

	public void setMaxSearchDeph(int maxSearchDeph) {
		this.maxSearchDeph = maxSearchDeph;
	}

	public String getDebugFolderPath() {
		return debugFolderPath;
	}

	public void setDebugFolderPath(String debugFolderPath) {
		this.debugFolderPath = debugFolderPath;
	}

}
