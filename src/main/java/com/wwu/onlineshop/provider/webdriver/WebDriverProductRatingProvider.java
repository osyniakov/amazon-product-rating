package com.wwu.onlineshop.provider.webdriver;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wwu.onlineshop.AmazonProductRatingConfig;
import com.wwu.onlineshop.provider.AmazonSearchContext;
import com.wwu.onlineshop.provider.AmazonSearchResult;
import com.wwu.onlineshop.provider.ProductRatingProvider;
import com.wwu.onlineshop.provider.webdriver.pageobject.AmazonMainPage;
import com.wwu.onlineshop.provider.webdriver.pageobject.AmazonResultsPage;
import com.wwu.onlineshop.provider.webdriver.util.WdUtil;

public class WebDriverProductRatingProvider implements ProductRatingProvider {

	private static final Logger LOG = LoggerFactory.getLogger(WebDriverProductRatingProvider.class);

	private WebDriver driver;
	private final String firefoxPath;
	private boolean initialized;

	public WebDriverProductRatingProvider(String firefoxPath) {
		this.firefoxPath = firefoxPath;
	}

	@Override
	public void initialize() {
		if (!initialized) {
			File pathToBinary = new File(firefoxPath);
			FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
			FirefoxProfile firefoxProfile = new FirefoxProfile();
			firefoxProfile.setPreference("browser.private.browsing.autostart", true);
			this.driver = new FirefoxDriver(ffBinary, firefoxProfile);
//			driver.manage().window().setPosition(new Point(-2000, 0));
			initialized = true;
		}
	}

	@Override
	public void shutdown() {
		try {
			if (driver != null) {
				driver.quit();
			}
		} catch (Exception e) {
			LOG.error("Error while quitting driver" ,e);
		}
	}

	@Override
	public AmazonSearchResult getResult(AmazonSearchContext searchContext) {
		try {
			AmazonResultsPage results = new AmazonMainPage(driver).search(searchContext);
			return results.getResult();
		} catch (Exception e) {
			if (AmazonProductRatingConfig.INSTANCE.isDebugMode()) {
				WdUtil.takeScreenshot(driver, WdUtil.prepareScreenshotPath(searchContext.getDebugFolderPath(), searchContext.getLineNum(), "error"));
			}
			return AmazonSearchResult.UNDEFINED;
		}
	}

}
