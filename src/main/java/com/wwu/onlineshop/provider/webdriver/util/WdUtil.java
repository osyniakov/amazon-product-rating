package com.wwu.onlineshop.provider.webdriver.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WdUtil {

	private static final Logger LOG = LoggerFactory.getLogger(WdUtil.class);

	private static int WAIT_TIMEOUT_SEC = 60;

	public static String prepareScreenshotPath(String folderPath, int lineNum, String fileName) {
		return folderPath + String.valueOf(lineNum) + "_" + fileName + ".png";
	}

	public static void takeScreenshot(WebDriver driver, String path) {
		File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenShot, new File(path));
		} catch (IOException e) {
			LOG.error("Error while writing screenshot", e);
		}
	}

	public static void hightlightElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', 'background-color:red')", element);
	}

	public static WebDriverWait waitFor(WebDriver driver) {
		return new WebDriverWait(driver, WAIT_TIMEOUT_SEC);
	}

}
