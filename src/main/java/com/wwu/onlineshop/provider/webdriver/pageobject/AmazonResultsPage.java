package com.wwu.onlineshop.provider.webdriver.pageobject;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.wwu.onlineshop.AmazonProductRatingConfig;
import com.wwu.onlineshop.provider.AmazonSearchContext;
import com.wwu.onlineshop.provider.AmazonSearchResult;
import com.wwu.onlineshop.provider.AmazonSearchResult.Status;
import com.wwu.onlineshop.provider.AmazonSearchResult.Typ;
import com.wwu.onlineshop.provider.webdriver.util.WdUtil;

public class AmazonResultsPage implements Iterable<AmazonResultsPage> {

	private final By navigationSelector = By.id("pagn");
	private final By nextPageSelector = By.id("pagnNextLink");
	private final By resultsSelector = By.cssSelector(".s-result-item");

	private static final String DATA_ASIN_ATTR = "data-asin";
	private static final String SPONSORED_ITEM_CLASS = "s-hidden-sponsored-item";

	private AmazonSearchContext searchContext;
	private WebDriver driver;

	public AmazonResultsPage(WebDriver driver, AmazonSearchContext searchContext) {
		this.driver = driver;
		this.searchContext = searchContext;
	}

	public AmazonSearchResult getResult() {
		WdUtil.waitFor(driver).until(ExpectedConditions.visibilityOfElementLocated(navigationSelector));

		List<WebElement> results = driver.findElements(resultsSelector);
		for (WebElement result: results) {
			Typ typ = getResultTyp(result);

			if (getResultNum(result) > searchContext.getMaxSearchDeph()) {
				if (AmazonProductRatingConfig.INSTANCE.isDebugMode()) {
					WdUtil.takeScreenshot(driver, WdUtil.prepareScreenshotPath(searchContext.getDebugFolderPath(), searchContext.getLineNum(), "notfound"));
				}
				return new AmazonSearchResult(typ, Status.MAX_SEARCH_DEPTH_EXCEEDED);
			}

			String dataAsin = result.getAttribute(DATA_ASIN_ATTR);
			if (searchContext.getTargetProductAsin().equals(dataAsin)) {
				WdUtil.hightlightElement(driver, result);
				if (AmazonProductRatingConfig.INSTANCE.isDebugMode()) {
					WdUtil.takeScreenshot(driver, WdUtil.prepareScreenshotPath(searchContext.getDebugFolderPath(), searchContext.getLineNum(), "found"));
				}
				return new AmazonSearchResult(getResultNum(result), typ, Status.OK);
			}
		}

		AmazonSearchResult result = AmazonSearchResult.UNDEFINED;
		for (AmazonResultsPage nextPage: this) {
			result = nextPage.getResult();
			if (result.getStatus().isOk() || result.getStatus().isMaxSearchDepthExceeded()) {
				return result;
			}
		}

		if (AmazonProductRatingConfig.INSTANCE.isDebugMode()) {
			WdUtil.takeScreenshot(driver, WdUtil.prepareScreenshotPath(searchContext.getDebugFolderPath(), searchContext.getLineNum(), "notfound"));
		}
		return result;
	}

	@Override
	public Iterator<AmazonResultsPage> iterator() {
		return new Iterator<AmazonResultsPage>() {

			@Override
			public boolean hasNext() {
				return !driver.findElements(nextPageSelector).isEmpty();
			}

			@Override
			public AmazonResultsPage next() {
				driver.findElement(nextPageSelector).click();
				return new AmazonResultsPage(driver, searchContext);
			}

		};
	}

	private int getResultNum(WebElement result) {
		if (result.getAttribute("id").isEmpty()) {
			return AmazonSearchResult.UNDEFINED.getRating();
		}
		return Integer.valueOf(result.getAttribute("id").replace("result_", "")) + 1;
	}

	private Typ getResultTyp(WebElement result) {
		Typ typ = Typ.NORMAL;
		if (result.getAttribute("class").contains(SPONSORED_ITEM_CLASS)) {
			typ = Typ.SPONSORED;
		}
		return typ;
	}

}
