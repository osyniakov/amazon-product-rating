package com.wwu.onlineshop.provider.webdriver.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.wwu.onlineshop.provider.AmazonSearchContext;

public class AmazonSearchPanel {

	private WebDriver driver;

	public AmazonSearchPanel(WebDriver driver) {
		this.driver = driver;
	}

	public AmazonResultsPage search(AmazonSearchContext searchContext) {
		if (searchContext.getCategory() != null) {
			Select categorySelect = new Select(driver.findElement(By.id("searchDropdownBox")));
			categorySelect.selectByVisibleText(searchContext.getCategory());
		}

		driver.findElement(By.id("twotabsearchtextbox")).sendKeys(searchContext.getKeyword());
		driver.findElement(By.className("nav-search-submit")).click();
		return new AmazonResultsPage(driver, searchContext);
	}

}
