package com.wwu.onlineshop.provider.webdriver.pageobject;

import org.openqa.selenium.WebDriver;

import com.wwu.onlineshop.provider.AmazonSearchContext;

public class AmazonMainPage {

	private static final String AMAZON_START_PAGE_URL = "https://amazon.com";

	private WebDriver driver;
	private AmazonSearchPanel searchPanel;

	public AmazonMainPage(WebDriver driver) {
		this.driver = driver;
		this.searchPanel = new AmazonSearchPanel(driver);
	}

	public AmazonResultsPage search(AmazonSearchContext searchContext) {
		driver.navigate().to(AMAZON_START_PAGE_URL);
		return searchPanel.search(searchContext);
	}

}
