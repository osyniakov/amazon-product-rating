package com.wwu.onlineshop.provider;

public interface ProductRatingProvider {

	void initialize();

	void shutdown();

	AmazonSearchResult getResult(AmazonSearchContext searchContext);

}
