package com.wwu.onlineshop.provider;

public class AmazonSearchResult {

	public static final AmazonSearchResult UNDEFINED = new AmazonSearchResult(Typ.UNDEFINED, Status.UNDEFINED);

	private int rating;
	private Typ typ;
	private Status status;

	public AmazonSearchResult(Typ typ, Status status) {
		this(-1, typ, status);
	}

	public AmazonSearchResult(int rating, Typ typ, Status status) {
		this.rating = rating;
		this.typ = typ;
		this.status = status;
	}

	@Override
	public String toString() {
		return "AmazonSearchResult [rating=" + rating + ", "
				+ "typ=" + typ + ", "
				+ "status=" + status + "]";
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Typ getTyp() {
		return typ;
	}

	public void setTyp(Typ typ) {
		this.typ = typ;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public enum Typ {
		UNDEFINED, NORMAL, SPONSORED;

		public boolean isNormal() {
			return this == NORMAL;
		}

		public boolean isSponsored() {
			return this == SPONSORED;
		}
	}

	public enum Status {
		UNDEFINED, OK, MAX_SEARCH_DEPTH_EXCEEDED;

		public boolean isOk() {
			return this == OK;
		}

		public boolean isMaxSearchDepthExceeded() {
			return this == MAX_SEARCH_DEPTH_EXCEEDED;
		}
	}

}
