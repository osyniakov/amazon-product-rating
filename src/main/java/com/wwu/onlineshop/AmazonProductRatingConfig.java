package com.wwu.onlineshop;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum AmazonProductRatingConfig {

	INSTANCE;

	private String inFilePath;
	private boolean debugMode;
	private String debugFolderPath;
	private String firefoxPath;
	private int maxSearchDeph;
	private int threads;
	private String cronExpression;

	public void load(InputStream paramInputStream) throws IOException {
		Properties props = new Properties();
		props.load(paramInputStream);
		inFilePath = props.getProperty("inFilePath");
		debugFolderPath = props.getProperty("debugFolderPath");
		firefoxPath = props.getProperty("firefoxPath");
		maxSearchDeph = Integer.valueOf(props.getProperty("maxSearchDeph"));
		cronExpression = props.getProperty("cronExpression");
		debugMode = Boolean.valueOf(props.getProperty("debugMode"));
		threads = Integer.valueOf(props.getProperty("threads"));
	}

	public String getInFilePath() {
		return inFilePath;
	}

	public boolean isDebugMode() {
		return debugMode;
	}

	public String getDebugFolderPath() {
		return debugFolderPath;
	}

	public String getFirefoxPath() {
		return firefoxPath;
	}

	public int getMaxSearchDeph() {
		return maxSearchDeph;
	}

	public int getThreads() {
		return threads;
	}

	public String getCronExpression() {
		return cronExpression;
	}


}
